﻿// Copyright (C) 2019-2021 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

using System;
using UnityEngine;

namespace CCGKit
{
    /// <summary>
    /// The type corresponding to the "Gain X shield" card effect.
    /// </summary>
    [Serializable]
    public class GainShieldEffect : IntegerEffect, IEntityEffect
    {
        public override string GetName()
        {
            return $"获得 {Value} 点护甲值";
        }

        public override void Resolve(RuntimeCharacter instigator, RuntimeCharacter target)
        {
            var targetShield = target.Shield;
            var defend = instigator.Status.GetValue("Defend"); // 盾强层数
            var extraShield = (int)Mathf.Floor(0.5f * defend * Value);
            if (defend > 0)
            {
                targetShield.SetValue(targetShield.Value + Value + extraShield);
            }
            else
            {
                targetShield.SetValue(targetShield.Value + Value);
            }
            // 直接给Value赋值，效果永久保留
        }
    }
}

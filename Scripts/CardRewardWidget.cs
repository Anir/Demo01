// Copyright (C) 2019-2021 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

using System.Text;
using UnityEngine;

namespace CCGKit
{
    public class CardRewardWidget : MonoBehaviour
    {
        public Canvas Canvas;
        public Canvas PopupCanvas;

        private CardWidget cardWidget;

        public DeckDrawingSystem DeckDrawingSystem;

        private void Awake()
        {
            cardWidget = GetComponent<CardWidget>();
        }

        public void OnCardPressed()
        {
            var id =  cardWidget.Card.Id;
            var gameInfo = FindObjectOfType<GameInfo>();
            gameInfo.SaveData.Deck.Add(id);
            Canvas.gameObject.SetActive(false);
            PopupCanvas.gameObject.SetActive(true);
        }

        public void CardPressedtoUse() {
            var CardSelected = cardWidget.Card;
            DeckDrawingSystem.AppendDeck(CardSelected);
            Canvas.gameObject.SetActive(false);
            PopupCanvas.gameObject.SetActive(true);
        }
    }
}
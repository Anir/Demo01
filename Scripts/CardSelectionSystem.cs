﻿// Copyright (C) 2019-2021 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

using UnityEngine;

namespace CCGKit
{
    /// <summary>
    /// The base class of the card selection systems. It contains the shared code of both
    /// selection systems (with and without a targeting arrow).
    /// </summary>
    public abstract class CardSelectionSystem : BaseSystem
    {
        public IntVariable PlayerMana;

        public TurnManagementSystem TurnManagementSystem;
        public DeckDrawingSystem DeckDrawingSystem;
        public HandPresentationSystem HandPresentationSystem;
        public EffectResolutionSystem EffectResolutionSystem;

        protected Camera MainCamera;
        protected LayerMask CardLayer;
        protected LayerMask EnemyLayer;

        protected GameObject SelectedCard;

        public int ECOUNT, ACOUNT, ICOUNT, NCOUNT = 0;
        public CardRewardSystem LinkRewardSystemI;
        public LinkRewardSystem LinkRewardSystemN;
        public LinkRewardSystem LinkRewardSystemA;
        public LinkRewardSystem LinkRewardSystemE;

        protected virtual void Start()
        {
            CardLayer = 1 << LayerMask.NameToLayer("Card");
            EnemyLayer = 1 << LayerMask.NameToLayer("Enemy");
            MainCamera = Camera.main;
        }

        protected virtual void PlaySelectedCard()
        {
            var cardObject = SelectedCard.GetComponent<CardObject>();
            cardObject.SetInteractable(false);
            var cardTemplate = cardObject.Template;
            PlayerMana.SetValue(PlayerMana.Value - cardTemplate.Cost);
            switch (cardTemplate.Type.Belong) {
                case "Eve": ECOUNT++; break;
                case "Aurum": ACOUNT++; break;
                case "Ningirsu": NCOUNT++; break;
                case "Imduk": ICOUNT++; break;
            }

            if (ICOUNT >= 3) {
                LinkRewardSystemI.OnPlayerRedeemedReward();
                ResetCount();
            }

            if (NCOUNT >= 3)
            {
                LinkRewardSystemN.OnLinkReward();
                ResetCount();
            }

            if (ACOUNT >= 3)
            {
                LinkRewardSystemA.OnLinkReward();
                ResetCount();
            }

            if (ECOUNT >= 3)
            {
                LinkRewardSystemE.OnLinkReward();
                ResetCount();
            }

            HandPresentationSystem.RearrangeHand(SelectedCard);
            HandPresentationSystem.RemoveCardFromHand(SelectedCard);
            HandPresentationSystem.MoveCardToDiscardPile(SelectedCard);

            DeckDrawingSystem.MoveCardToDiscardPile(cardObject.RuntimeCard);
        }

        public void ResetCount() {
            ECOUNT = 0;
            ACOUNT = 0;
            NCOUNT = 0;
            ICOUNT = 0;
        }

        public bool HasSelectedCard()
        {
            return SelectedCard != null;
        }
    }
}

﻿// Copyright (C) 2019-2021 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.
using UnityEngine;

namespace CCGKit
{
    /// <summary>
    /// This system is responsible for managing the poison status on characters.
    /// It deals poison damage at the beginning of the turn, and reduces the poison
    /// stack at the end of the turn.
    /// </summary>
    public class PoisonResolutionSystem : BaseSystem
    {
        public void OnPlayerTurnBegan()
        {
            var player = Player.Character;
            var poison = player.Status.GetValue("Poison");
            var RP = player.Status.GetValue("RePoison");
            if (poison > 0)
            {
                var playerHp = player.Hp;
                playerHp.SetValue(playerHp.Value - poison * (RP + 1));
            }
        }

        public void OnPlayerTurnEnded()
        {
            var player = Player.Character;
            var poison = player.Status.GetValue("Poison");
            var STH = player.Status.GetValue("ShieldtoHp");
            if (STH > 0) {
                var playerHp = player.Hp;
                var playerShield = player.Shield;
                var extraHp = (int)(Mathf.Floor(playerShield.Value * 0.25f * STH));
                playerHp.SetValue(playerHp.Value + extraHp);
            }
            if (poison > 0)
            {
                player.Status.SetValue("Poison", poison-1);
            }
        }

        public void OnEnemyTurnBegan()
        {
            foreach (var enemy in Enemies)
            {
                var character = enemy.Character;
                var poison = character.Status.GetValue("Poison");
                var Aurum1 = character.Status.GetValue("Aurum1");
                if (poison > 0)
                {
                    var enemyHp = character.Hp;
                    if (enemyHp.Value > 0)
                    {
                        enemyHp.SetValue(enemyHp.Value - poison*(Aurum1+1));
                    }
                }
            }
        }

        public void OnEnemyTurnEnded()
        {
            foreach (var enemy in Enemies)
            {
                var character = enemy.Character;
                var poison = character.Status.GetValue("Poison");
                if (poison > 0)
                {
                    character.Status.SetValue("Poison", poison-1);
                }
            }
        }
    }
}
